from django.test import TestCase
from django.contrib.auth import get_user_model

class ModelTests(TestCase):

    def test_create_user_with_email_successful(self):
        """TEST CREATING A NEW USER WITH AN EMAIL IS SUCCESSFUL"""
        email = 'testmail@gmail.com'
        password = 'Magia123'
        user = get_user_model().objects.create_user(
            email = email,
            password = password
        )
        self.assertEqual(user.email,email)
        self.assertTrue(user.check_password(password))

    def test_new_user_email_normalized(self):
        """Test the email for a new User is Normalized"""
        email = 'testmail@GMAIL.COM'
        user = get_user_model().objects.create_user(email, 'Magia123')

        self.assertEqual(user.email, email.lower())

    def test_new_user_invalid_email(self):
        """Test Creating User with no email raises error"""
        with self.assertRaises(ValueError):
            get_user_model().objects.create_user(None, 'Magia123')

    def test_create_new_superuser(self):
        """Test creating a new superuser"""
        user = get_user_model().objects.create_superuser(
            'testmail@gmail.com',
            'Magia123'
        )

        self.assertTrue(user.is_superuser)
        self.assertTrue(user.is_staff)