from django.test import TestCase, Client
from django.contrib.auth import get_user_model
from django.urls import reverse


# Generacion de Urls ^

class AdminSiteTests(TestCase):

    # Set Up Function for the Others Tests
    def setUp(self):
        self.client = Client()
        self.admin_user = get_user_model().objects.create_superuser(
            email='testmail@gmail.com',
            password='Magia123'
        )
        self.client.force_login(self.admin_user)
        self.user = get_user_model().objects.create_user(
            email='testuser@gmail.com',
            password='Magia123',
            name='Nmae of Test User'
        )

    def test_users_listed(self):
        """Test that users are listed on the admin user page"""
        url = reverse('admin:core_user_changelist')
        res = self.client.get(url)

        # Verificamos que exista el usuario creado normal en la response de admin
        self.assertContains(res, self.user.name)
        self.assertContains(res, self.user.email)

    def test_user_change_page(self):
        """Test that the user edit page works"""
        url = reverse('admin:core_user_change', args=[self.user.id])
        # Url Generada: /admin/core/user/:id
        res = self.client.get(url)
        
        self.assertEqual(res.status_code, 200)

    def test_create_user_page(self):
        """Test that the create user page works"""
        url = reverse('admin:core_user_add')
        res = self.client.get(url)
        
        self.assertEqual(res.status_code, 200)
