from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin


class UserManager(BaseUserManager):

    def create_user(self, email, password=None, **extra_fields):
        """CREATES AND SAVES A NEW USER"""
        # Validations are meant to be made in the Serializers but in this time we made it here to show
        if not email:
            raise ValueError('Users must have an email address')
        user = self.model(email=self.normalize_email(email), **extra_fields)
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, email, password):
        """Creates and Saves a new SuperUser"""
        user = self.create_user(email,password)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)

        return user


class User(AbstractBaseUser, PermissionsMixin):
    """CUSTOM USER MODEL THAT SUPPORTS USING EMAIL INSTEAD OF USERNAME"""
    email = models.EmailField(max_length=255, unique=True)
    name = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    # Para que el objeto de usuario sea manejao tambien por el user manager
    objects = UserManager()

    USERNAME_FIELD = 'email'
